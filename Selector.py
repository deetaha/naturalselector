import string
import random

class Selector:
	SetOfPermissibleCharacters = string.printable
	
	def __init__(self, GoalString, NumberOfOffspring = 100, NumberOfCharactersChanged = 1, ParentAmongOffspring = True):
		self.__GoalString = GoalString
		self.__LengthOfString = len(GoalString)
		self.__NumberOfOffspring = NumberOfOffspring
		self.__NumberOfCharactersChanged = NumberOfCharactersChanged
		self.__CurrentBaby = self.__RandomString()
		self.__ParentAmongOffspring = ParentAmongOffspring

	def GetCurrentBaby(self):
		return self.__CurrentBaby
			
	def __RandomString(self):
		RandomString = ''
		
		for i in xrange(self.__LengthOfString):
			RandomString += random.choice(self.SetOfPermissibleCharacters)
		
		return RandomString
		
	def __DistanceFromGoal(self, String):
		HammingDistance = 0
		
		for i in xrange(self.__LengthOfString):
			if String[i] != self.__GoalString[i]:
				HammingDistance += 1
		
		return HammingDistance
		
	def GetCurrentDistanceFromGoal(self):
		return self.__DistanceFromGoal(self.__CurrentBaby)

	def __Breed(self):
		Offspring = []
		
		for i in xrange(self.__NumberOfOffspring):
			BabyString = list(self.__CurrentBaby)
			LocationOfCharactersToBeChanged = random.sample(xrange(self.__LengthOfString), self.__NumberOfCharactersChanged)
		
			for Location in LocationOfCharactersToBeChanged:
				OldCharacter = BabyString[Location]
		
				while BabyString[Location] == OldCharacter:
					BabyString[Location] = random.choice(self.SetOfPermissibleCharacters)
		
			BabyString = ''.join(BabyString)
		
			Offspring.append(BabyString)
		
		return Offspring
			
	def BreedAndSelect(self):
		Offspring = self.__Breed()
		
		if self.__ParentAmongOffspring:
			FittestBaby = self.__CurrentBaby
		else:
			FittestBaby = Offspring[0]

		for Baby in Offspring:
			if self.__DistanceFromGoal(Baby) < self.__DistanceFromGoal(FittestBaby):
				FittestBaby = Baby
				
		self.__CurrentBaby = FittestBaby





# Sample Run

if __name__ == '__main__':
	TextCorpus = '"Would you tell me, please, which way I ought to go from here?"\n"That depends a good deal on where you want to get to," said the Cat.\n"I don\'t much care where--" said Alice.\n"Then it doesn\'t matter which way you go," said the Cat.\n"--so long as I get SOMEWHERE," Alice added as an explanation.\n"Oh, you\'re sure to do that," said the Cat, "if you only walk long enough."'
	
	print "***********************************"
	print "**********[ORIGINAL TEXT]**********"
	print "***********************************"
	print TextCorpus
	print
	print "Source: Caroll, Lewis. Alice in Wonderland."
	print
	
	Experiment = Selector(TextCorpus)
	
	print "******************************************"
	print "**********[ORIGINAL RANDOM SEED]**********"
	print "******************************************"
	print Experiment.GetCurrentBaby()
	print
	
	Step = 0
	while True:
		Experiment.BreedAndSelect()
		
		Step += 1
		
		CurrentBaby = Experiment.GetCurrentBaby()
		DistanceFromGoal = Experiment.GetCurrentDistanceFromGoal()
		
		print "**********[STEP:", Step, ", DISTANCE FROM ORIGINAL TEXT:", DistanceFromGoal, "character(s)]**********"
		print CurrentBaby
		print
		
		if DistanceFromGoal == 0:
			break